import datetime
import time_machine

@time_machine.travel("1980-02-03 10:00")
def test_basics():
    assert datetime.date.today().year == 1980
